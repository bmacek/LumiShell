#ifndef INCLUDE_LUMIDETPERIOD_INCLUDED
#define INCLUDE_LUMIDETPERIOD_INCLUDED

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"

#include "LumiShell/common/defines.h"

namespace LumiShell {

class DetPeriod : public CatShell::CatObject
///
/// Accumulation period of any type of data for a given detector.
///
{
        CAT_OBJECT_DECLARE(DetPeriod, CatShell::CatObject, CAT_NAME_LUMI_DETPERIOD, CAT_TYPE_LUMI_DETPERIOD);
public:

        DetPeriod();
                /// Constructor: default.

        virtual ~DetPeriod();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        void set_start(CatShell::TimePoint time);
                /// Sets the start of the period.

        void set_end(CatShell::TimePoint time);
                /// Sets the end of the period.

        const CatShell::TimePoint& get_start() const;
                /// Returns the start time of the period.

        const CatShell::TimePoint& get_end() const;
                /// Returns the end time of the period.

protected:

private:

        CatShell::TimePoint     _start;
        CatShell::TimePoint     _end;
};

inline void DetPeriod::cat_free() {}
inline StreamSize DetPeriod::cat_stream_get_size() const { return _start.get_stream_length() + _end.get_stream_length(); }
inline void DetPeriod::set_start(CatShell::TimePoint time) { _start = time; }
inline void DetPeriod::set_end(CatShell::TimePoint time) { _end = time; }
inline const CatShell::TimePoint& DetPeriod::get_start() const { return _start; }
inline const CatShell::TimePoint& DetPeriod::get_end() const { return _end; }
inline void* DetPeriod::cat_get_part(CatShell::CatAddress& addr) { return nullptr; }

} // namespace LumiShell

#endif

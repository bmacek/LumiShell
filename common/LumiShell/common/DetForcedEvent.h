#ifndef INCLUDE_LUMIDETFORCEDEVENT_INCLUDED
#define INCLUDE_LUMIDETFORCEDEVENT_INCLUDED

#include "CatShell/core/CatObject.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetEvent.h"

namespace LumiShell {

class DetForcedEvent : public DetEvent
///
/// Forced event, containing string message - for general adaptability.
///
{
        CAT_OBJECT_DECLARE(DetForcedEvent, DetEvent, CAT_NAME_LUMI_DETFORCEDEVENT, CAT_TYPE_LUMI_DETFORCEDEVENT);
public:

        DetForcedEvent();
                /// Constructor: default.

        virtual ~DetForcedEvent();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        DetForcedEvent& operator=(const std::string& text);
                /// Assign text to the event.

protected:

private:

        std::string     _message;
};

inline void DetForcedEvent::cat_free() {}
inline StreamSize DetForcedEvent::cat_stream_get_size() const { return DetEvent::cat_stream_get_size() + sizeof(std::uint32_t) + _message.length(); }
inline DetForcedEvent& DetForcedEvent::operator=(const std::string& text) { _message = text; return *this; }

} // namespace LumiShell

#endif

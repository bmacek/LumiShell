#ifndef INCLUDE_LUMIDETNOTHING_INCLUDED
#define INCLUDE_LUMIDETNOTHING_INCLUDED

#include "CatShell/core/CatObject.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetEvent.h"

namespace LumiShell {

class DetNothing : public DetEvent
///
/// Nothing happened.
///
{
        CAT_OBJECT_DECLARE(DetNothing, DetEvent, CAT_NAME_LUMI_DETNOTHING, CAT_TYPE_LUMI_DETNOTHING);
public:

        DetNothing();
                /// Constructor: default.

        virtual ~DetNothing();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

protected:

private:

};

inline void DetNothing::cat_free() {}
inline StreamSize DetNothing::cat_stream_get_size() const { return DetEvent::cat_stream_get_size(); }

} // namespace LumiShell

#endif

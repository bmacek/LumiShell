#ifndef INCLUDE_LUMIDETEVENT_INCLUDED
#define INCLUDE_LUMIDETEVENT_INCLUDED

#include "CatShell/core/CatObject.h"

#include "LumiShell/common/defines.h"

namespace LumiShell {

class DetEvent : public CatShell::CatObject
///
/// Event notable in lumi-stream.
///
{
        CAT_OBJECT_DECLARE(DetEvent, CatShell::CatObject, CAT_NAME_LUMI_DETEVENT, CAT_TYPE_LUMI_DETEVENT);
public:

        DetEvent();
                /// Constructor: default.

        virtual ~DetEvent();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

protected:

private:

};

inline void DetEvent::cat_free() {}
inline StreamSize DetEvent::cat_stream_get_size() const { return 0; }

} // namespace LumiShell

#endif

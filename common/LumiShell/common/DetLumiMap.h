#ifndef INCLUDE_LUMIDETLUMIMAP_INCLUDED
#define INCLUDE_LUMIDETLUMIMAP_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetPeriod.h"

namespace LumiShell {

class DetLumiMap : public DetPeriod
///
/// Accumulation period with luminosity information.
///
{
        CAT_OBJECT_DECLARE(DetLumiMap, DetPeriod, CAT_NAME_LUMI_DETLUMIMAP, CAT_TYPE_LUMI_DETLUMIMAP);
public:

        typedef struct {
                double value;
                double error;
        } tBcid;

        DetLumiMap();
                /// Constructor: default.

        virtual ~DetLumiMap();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        tBcid operator[](int index);
                /// Return reference to the data of single BC.

        double& get_value(int index);
                /// Return value.

        double& get_error(int index);
                /// Return error.

        void set_map_id(std::uint32_t id);
                /// Sets the ID of the map.

        std::uint32_t get_map_id() const;
                /// Returns the ID of the map.

        void set_orbits(double orbits);
                /// Sets the number of orbits.

        double get_orbits() const;
                /// Returns the number of orbits.

protected:

private:

        std::uint32_t   _map_id;
        double          _orbits;
        double          _bcid_value[ORBIT_LENGTH];
        double          _bcid_error[ORBIT_LENGTH];
};

inline void DetLumiMap::cat_free() {}
inline void DetLumiMap::set_map_id(std::uint32_t id) { _map_id = id; }
inline void DetLumiMap::set_orbits(double orbits) { _orbits = orbits; }
inline std::uint32_t DetLumiMap::get_map_id() const { return _map_id; }
inline double DetLumiMap::get_orbits() const { return _orbits; }
inline double& DetLumiMap::get_value(int index) { return _bcid_value[index]; }
inline double& DetLumiMap::get_error(int index) { return _bcid_error[index]; }

} // namespace LumiShell

#endif

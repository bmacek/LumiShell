#ifndef INCLUDE_LUMIDETLBCHANGE_INCLUDED
#define INCLUDE_LUMIDETLBCHANGE_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/TimePoint.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetEvent.h"

namespace LumiShell {

class DetLbChange : public DetEvent
///
/// Accumulation period for BCM.
///
{
        CAT_OBJECT_DECLARE(DetLbChange, DetEvent, CAT_NAME_LUMI_DETLBCHANGE, CAT_TYPE_LUMI_DETLBCHANGE);
public:

        DetLbChange();
                /// Constructor: default.

        virtual ~DetLbChange();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        void set_run(std::uint32_t run);
                /// Sets the run number.

        void set_lumi_block(std::uint16_t lb);
                /// Sets the number of lumi block.

        void set_time(const CatShell::TimePoint& time);
                /// Sets the timestamp of lb change.

        std::uint32_t get_run() const;
                /// Returns the run number.

protected:

private:

        std::uint32_t           _run_number;
        std::uint16_t           _lumi_block;
        CatShell::TimePoint     _time;
};

inline void DetLbChange::cat_free() {}
inline StreamSize DetLbChange::cat_stream_get_size() const { return DetEvent::cat_stream_get_size() + _time.get_stream_length() + sizeof(std::uint32_t) + sizeof(std::uint16_t); }
inline void DetLbChange::set_run(std::uint32_t run) { _run_number = run; }
inline void DetLbChange::set_lumi_block(std::uint16_t lb) { _lumi_block = lb; }
inline void DetLbChange::set_time(const CatShell::TimePoint& time) { _time = time; }
inline std::uint32_t DetLbChange::get_run() const { return _run_number; }

} // namespace LumiShell

#endif

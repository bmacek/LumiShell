#ifndef INCLUDE_LUMIDETLUMICOUNTERS_INCLUDED
#define INCLUDE_LUMIDETLUMICOUNTERS_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetPeriod.h"

namespace LumiShell {

class DetLumiCounters : public DetPeriod
///
/// Accumulation period with raw single-counter luminosity information.
///
{
        CAT_OBJECT_DECLARE(DetLumiCounters, DetPeriod, CAT_NAME_LUMI_DETLUMICOUNTERS, CAT_TYPE_LUMI_DETLUMICOUNTERS);
public:

        DetLumiCounters();
                /// Constructor: default.

        virtual ~DetLumiCounters();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        std::uint32_t& operator[](int index);
                /// Return reference to the data of single BC.

        void set_map_id(std::uint32_t id);
                /// Sets the ID of the map.

        std::uint32_t get_map_id() const;
                /// Returns the ID of the map.

        void set_orbits(std::uint32_t orbits);
                /// Sets the number of orbits.

        std::uint32_t get_orbits() const;
                /// Returns the number of orbits.

protected:

private:

        std::uint32_t   _map_id;
        std::uint32_t   _orbits;
        std::uint32_t   _bcid[ORBIT_LENGTH];
};

inline void DetLumiCounters::cat_free() {}
inline void DetLumiCounters::set_map_id(std::uint32_t id) { _map_id = id; }
inline void DetLumiCounters::set_orbits(std::uint32_t orbits) { _orbits = orbits; }
inline std::uint32_t DetLumiCounters::get_map_id() const { return _map_id; }
inline std::uint32_t DetLumiCounters::get_orbits() const { return _orbits; }
inline std::uint32_t& DetLumiCounters::operator[](int index) { return _bcid[index]; }

} // namespace LumiShell

#endif

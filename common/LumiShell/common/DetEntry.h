#ifndef INCLUDE_LUMIDETENTRY_INCLUDED
#define INCLUDE_LUMIDETENTRY_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetEvent.h"
#include "LumiShell/common/DetPeriod.h"

namespace LumiShell {

class DetEntry : public CatShell::CatObject
///
/// Luminosity entity, with period and events.
///
{
        CAT_OBJECT_DECLARE(DetEntry, CatShell::CatObject, CAT_NAME_LUMI_DETENTRY, CAT_TYPE_LUMI_DETENTRY);
public:

        DetEntry();
                /// Constructor: default.

        virtual ~DetEntry();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.

//----------------------------------------------------------------------

        void loop_events(std::function<void (CatShell::CatPointer<DetEvent>&)> f);
                /// Calls the function for all events.

        void insert_event(CatShell::CatPointer<DetEvent>& event);
                /// Insert event in the list of events.

        CatShell::CatPointer<DetPeriod>& get_period();
                /// Returns the pointer to the period.

        void set_entry_id(std::uint32_t id);
                /// Sets the unique id.

        std::uint32_t get_entry_id() const;
                /// Retutrns entry id.

protected:

private:

        std::uint32_t                                   _entry_id;
        CatShell::CatPointer<DetPeriod>                 _period;
        std::vector<CatShell::CatPointer<DetEvent>>     _events;

};

inline void DetEntry::loop_events(std::function<void (CatShell::CatPointer<DetEvent>&)> f) { for(auto& x: _events) f(x); }
inline void DetEntry::insert_event(CatShell::CatPointer<DetEvent>& event) { _events.push_back(event); }
inline CatShell::CatPointer<DetPeriod>& DetEntry::get_period() { return _period; }
inline void DetEntry::set_entry_id(std::uint32_t id) { _entry_id = id; }
inline std::uint32_t DetEntry::get_entry_id() const { return _entry_id; }

} // namespace LumiShell

#endif

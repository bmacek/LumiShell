#ifndef INCLUDE_LUMIDETPERIODBLANK_INCLUDED
#define INCLUDE_LUMIDETPERIODBLANK_INCLUDED

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"

#include "LumiShell/common/defines.h"
#include "LumiShell/common/DetPeriod.h"

namespace LumiShell {

class DetPeriodBlank : public DetPeriod
///
/// Accumulation period where no data could be found.
///
{
        CAT_OBJECT_DECLARE(DetPeriodBlank, DetPeriod, CAT_NAME_LUMI_DETPERIODBLANK, CAT_TYPE_LUMI_DETPERIODBLANK);
public:

        DetPeriodBlank();
                /// Constructor: default.

        virtual ~DetPeriodBlank();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

protected:

private:
};

inline void DetPeriodBlank::cat_free() {}
inline StreamSize DetPeriodBlank::cat_stream_get_size() const { return DetPeriod::cat_stream_get_size(); }

} // namespace LumiShell

#endif

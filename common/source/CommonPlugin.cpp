#include "CatShell/core/Plugin.h"
#include "LumiShell/common/DetPeriod.h"
#include "LumiShell/common/DetLumiMap.h"
#include "LumiShell/common/DetEvent.h"
#include "LumiShell/common/DetNothing.h"
#include "LumiShell/common/DetPlbChange.h"
#include "LumiShell/common/DetLbChange.h"
#include "LumiShell/common/DetForcedEvent.h"
#include "LumiShell/common/DetEntry.h"
#include "LumiShell/common/DetPeriodBlank.h"
#include "LumiShell/common/DetLumiCounters.h"

using namespace std;

namespace LumiShell {

EXPORT_PLUGIN(CatShell::Plugin, LumiCommonPlugin)
START_EXPORT_CLASSES(LumiCommonPlugin)
EXPORT_CLASS(DetPeriod, CAT_NAME_LUMI_DETPERIOD, LumiCommonPlugin)
EXPORT_CLASS(DetLumiMap, CAT_NAME_LUMI_DETLUMIMAP, LumiCommonPlugin)
EXPORT_CLASS(DetEvent, CAT_NAME_LUMI_DETEVENT, LumiCommonPlugin)
EXPORT_CLASS(DetNothing, CAT_NAME_LUMI_DETNOTHING, LumiCommonPlugin)
EXPORT_CLASS(DetPlbChange, CAT_NAME_LUMI_DETPLBCHANGE, LumiCommonPlugin)
EXPORT_CLASS(DetLbChange, CAT_NAME_LUMI_DETLBCHANGE, LumiCommonPlugin)
EXPORT_CLASS(DetForcedEvent, CAT_NAME_LUMI_DETFORCEDEVENT, LumiCommonPlugin)
EXPORT_CLASS(DetEntry, CAT_NAME_LUMI_DETENTRY, LumiCommonPlugin)
EXPORT_CLASS(DetPeriodBlank, CAT_NAME_LUMI_DETPERIODBLANK, LumiCommonPlugin)
EXPORT_CLASS(DetLumiCounters, CAT_NAME_LUMI_DETLUMICOUNTERS, LumiCommonPlugin)
END_EXPORT_CLASSES(LumiCommonPlugin)

} // namespace LumiShell
#include "LumiShell/common/DetLumiCounters.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetLumiCounters, CAT_NAME_LUMI_DETLUMICOUNTERS, CAT_TYPE_LUMI_DETLUMICOUNTERS);

DetLumiCounters::DetLumiCounters()
{
}

DetLumiCounters::~DetLumiCounters()
{
}

void DetLumiCounters::cat_stream_out(std::ostream& output)
{
	DetPeriod::cat_stream_out(output);

	output.write((char*)&_map_id, sizeof(std::uint32_t));
	output.write((char*)&_orbits, sizeof(std::uint32_t));
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
	{
		output.write((char*)&(_bcid[i]), sizeof(std::uint32_t));
	}
}

void DetLumiCounters::cat_stream_in(std::istream& input)
{
	DetPeriod::cat_stream_in(input);

	input.read((char*)&_map_id, sizeof(std::uint32_t));
	input.read((char*)&_orbits, sizeof(std::uint32_t));
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
	{
		input.read((char*)&(_bcid[i]), sizeof(std::uint32_t));
	}
}

void DetLumiCounters::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	DetPeriod::cat_print(output, padding); output << endl << padding << " Luminosity map." << endl;
	output << padding << "Map Id   : " << (int)_map_id << endl;
	output << padding << "Orbits   : " << _orbits << endl;
	output << padding << "Values : ";

	std::uint16_t i=0;
	for(std::uint16_t r=0; i<ORBIT_LENGTH; ++r)
	{
		sprintf(text, "%5u: ", i);
		output << endl << padding << text;
		for(; i<5*r+5 && i<ORBIT_LENGTH;i++)
		{
			sprintf(text, "%10u, ", _bcid[i]);
			output << text;
		}
	}
}

StreamSize DetLumiCounters::cat_stream_get_size() const
{ 
	return ORBIT_LENGTH*sizeof(std::uint32_t) + 2*sizeof(std::uint32_t);
}

void* DetLumiCounters::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "id")
	{
		return &_map_id;
	}
	else if(addr == "orbits")
	{
		return &_orbits;
	}
	else if(addr == "bcid")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= ORBIT_LENGTH)
                        return nullptr;
                else
                		return &(_bcid[addr.get_index()]);
        }
        else
                return nullptr;
	}
	else if(addr == "values")
	{
		return &(_bcid[0]);
	}
	else
		return nullptr;
}

} // namespace LumiShell

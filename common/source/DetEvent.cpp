#include "LumiShell/common/DetEvent.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetEvent, CAT_NAME_LUMI_DETEVENT, CAT_TYPE_LUMI_DETEVENT);

DetEvent::DetEvent()
{
}

DetEvent::~DetEvent()
{
}

void DetEvent::cat_stream_out(std::ostream& output)
{
}

void DetEvent::cat_stream_in(std::istream& input)
{
}

void DetEvent::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding);
}

} // namespace LumiShell

#include "LumiShell/common/DetLbChange.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetLbChange, CAT_NAME_LUMI_DETLBCHANGE, CAT_TYPE_LUMI_DETLBCHANGE);

DetLbChange::DetLbChange()
{
}

DetLbChange::~DetLbChange()
{
}

void DetLbChange::cat_stream_out(std::ostream& output)
{
	DetEvent::cat_stream_out(output);

	output.write((char*)&_run_number, sizeof(std::uint32_t));
	output.write((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.write_to_stream(output);
}

void DetLbChange::cat_stream_in(std::istream& input)
{
	DetEvent::cat_stream_in(input);

	input.read((char*)&_run_number, sizeof(std::uint32_t));
	input.read((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.read_from_stream(input);
}

void DetLbChange::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	DetEvent::cat_print(output, padding); output << "Lumi LB change." << endl;
	output << padding << "Run number     : " << _run_number << endl;
	output << padding << "Lumi block     : " << _lumi_block << endl;
	_time.convert_to_UTC_asci(text);
	output << padding << "Timestamp (UTC): " << text;
}

void* DetLbChange::cat_get_part(CatAddress& addr)
{
        if(addr == "run")
                return &_run_number;
        else if(addr == "block")
                return &_lumi_block;
        else
                return nullptr;
}

} // namespace LumiShell

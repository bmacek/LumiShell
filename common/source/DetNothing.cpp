#include "LumiShell/common/DetNothing.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetNothing, CAT_NAME_LUMI_DETNOTHING, CAT_TYPE_LUMI_DETNOTHING);

DetNothing::DetNothing()
{
}

DetNothing::~DetNothing()
{
}

void DetNothing::cat_stream_out(std::ostream& output)
{
	DetEvent::cat_stream_out(output);
}

void DetNothing::cat_stream_in(std::istream& input)
{
	DetEvent::cat_stream_in(input);
}

void DetNothing::cat_print(std::ostream& output, const std::string& padding)
{
	DetEvent::cat_print(output, padding); output << " Nothing happened.";
}

} // namespace LumiShell

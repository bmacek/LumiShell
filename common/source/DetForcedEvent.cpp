#include "LumiShell/common/DetForcedEvent.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetForcedEvent, CAT_NAME_LUMI_DETFORCEDEVENT, CAT_TYPE_LUMI_DETFORCEDEVENT);

DetForcedEvent::DetForcedEvent()
{
}

DetForcedEvent::~DetForcedEvent()
{
}

void DetForcedEvent::cat_stream_out(std::ostream& output)
{
	DetEvent::cat_stream_out(output);

	std::uint32_t tmp = _message.length();
	output.write((char*)&tmp, sizeof(std::uint32_t));
	output.write((char*)_message.c_str(), tmp);
}

void DetForcedEvent::cat_stream_in(std::istream& input)
{
	DetEvent::cat_stream_in(input);

	std::uint32_t tmp;
	input.read((char*)&tmp, sizeof(std::uint32_t));
	
	char* c = new char[tmp+1];
	input.read(c, tmp);
	c[tmp] = '\0';
	_message = c;
}

void DetForcedEvent::cat_print(std::ostream& output, const std::string& padding)
{
	DetEvent::cat_print(output, padding); output << " " << _message;
}

} // namespace LumiShell

#include "LumiShell/common/DetPeriod.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetPeriod, CAT_NAME_LUMI_DETPERIOD, CAT_TYPE_LUMI_DETPERIOD);

DetPeriod::DetPeriod()
{
}

DetPeriod::~DetPeriod()
{
}

void DetPeriod::cat_stream_out(std::ostream& output)
{
	_start.write_to_stream(output);
	_end.write_to_stream(output);
}

void DetPeriod::cat_stream_in(std::istream& input)
{
	_start.read_from_stream(input);
	_end.read_from_stream(input);
}

void DetPeriod::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	CatObject::cat_print(output, padding); output << " Detector period." << endl;
	_start.convert_to_UTC_asci(text);
	output << padding << "Timestamp start (UTC) : " << text << endl;
	_end.convert_to_UTC_asci(text);
	output << padding << "Timestamp end (UTC)   : " << text;
}

} // namespace LumiShell

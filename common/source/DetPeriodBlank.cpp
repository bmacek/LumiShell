#include "LumiShell/common/DetPeriodBlank.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetPeriodBlank, CAT_NAME_LUMI_DETPERIODBLANK, CAT_TYPE_LUMI_DETPERIODBLANK);

DetPeriodBlank::DetPeriodBlank()
{
}

DetPeriodBlank::~DetPeriodBlank()
{
}

void DetPeriodBlank::cat_stream_out(std::ostream& output)
{
	DetPeriod::cat_stream_out(output);
}

void DetPeriodBlank::cat_stream_in(std::istream& input)
{
	DetPeriod::cat_stream_in(input);
}

void DetPeriodBlank::cat_print(std::ostream& output, const std::string& padding)
{
	DetPeriod::cat_print(output, padding); output << " Lumi period with no data." << endl;
}

} // namespace LumiShell

#include "LumiShell/common/DetLumiMap.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetLumiMap, CAT_NAME_LUMI_DETLUMIMAP, CAT_TYPE_LUMI_DETLUMIMAP);

DetLumiMap::DetLumiMap()
{
}

DetLumiMap::~DetLumiMap()
{
}

void DetLumiMap::cat_stream_out(std::ostream& output)
{
	DetPeriod::cat_stream_out(output);

	output.write((char*)&_map_id, sizeof(std::uint32_t));
	output.write((char*)&_orbits, sizeof(double));
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
	{
		output.write((char*)&(_bcid_value[i]), sizeof(double));
		output.write((char*)&(_bcid_error[i]), sizeof(double));
	}
}

void DetLumiMap::cat_stream_in(std::istream& input)
{
	DetPeriod::cat_stream_in(input);

	input.read((char*)&_map_id, sizeof(std::uint32_t));
	input.read((char*)&_orbits, sizeof(double));
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
	{
		input.read((char*)&(_bcid_value[i]), sizeof(double));
		input.read((char*)&(_bcid_error[i]), sizeof(double));
	}
}

void DetLumiMap::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	DetPeriod::cat_print(output, padding); output << endl << padding << " Luminosity map." << endl;
	output << padding << "Map Id   : " << (int)_map_id << endl;
	output << padding << "Orbits   : " << _orbits << endl;
	output << padding << "Values : ";

	std::uint16_t i=0;
	for(std::uint16_t r=0; i<ORBIT_LENGTH; ++r)
	{
		sprintf(text, "%5u: ", i);
		output << endl << padding << text;
		for(; i<5*r+5 && i<ORBIT_LENGTH;i++)
		{
			sprintf(text, "%f +- %f, ", _bcid_value[i], _bcid_error[i]);
			output << text;
		}
	}
}

StreamSize DetLumiMap::cat_stream_get_size() const
{ 
	return ORBIT_LENGTH*2*sizeof(double) + sizeof(double) + 1*sizeof(std::uint32_t);
}

void* DetLumiMap::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "id")
	{
		return &_map_id;
	}
	else if(addr == "orbits")
	{
		return &_orbits;
	}
	else if(addr == "bcid")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= ORBIT_LENGTH)
                        return nullptr;
                else
                		return &(_bcid_value[addr.get_index()]);
        }
        else
                return nullptr;
	}
	else if(addr == "bcid_err")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= ORBIT_LENGTH)
                        return nullptr;
                else
                		return &(_bcid_error[addr.get_index()]);
        }
        else
                return nullptr;
	}
	else if(addr == "values")
	{
		return &_bcid_value;
	}
	else if(addr == "errors")
	{
		return &_bcid_error;
	}
	else
		return nullptr;
}

DetLumiMap::tBcid DetLumiMap::operator[](int index)
{
	tBcid tmp; 
	tmp.value = _bcid_value[index];
	tmp.error = _bcid_error[index];
	return tmp;
}

} // namespace LumiShell

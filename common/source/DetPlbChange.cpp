#include "LumiShell/common/DetPlbChange.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetPlbChange, CAT_NAME_LUMI_DETPLBCHANGE, CAT_TYPE_LUMI_DETPLBCHANGE);

DetPlbChange::DetPlbChange()
{
}

DetPlbChange::~DetPlbChange()
{
}

void DetPlbChange::cat_stream_out(std::ostream& output)
{
	DetEvent::cat_stream_out(output);

	output.write((char*)&_run_number, sizeof(std::uint32_t));
	output.write((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.write_to_stream(output);
}

void DetPlbChange::cat_stream_in(std::istream& input)
{
	DetEvent::cat_stream_in(input);

	input.read((char*)&_run_number, sizeof(std::uint32_t));
	input.read((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.read_from_stream(input);
}

void DetPlbChange::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	DetEvent::cat_print(output, padding); output << "Lumi Pseudo-LB change." << endl;
	output << padding << "Run number     : " << _run_number << endl;
	output << padding << "Lumi block     : " << _lumi_block << endl;
	_time.convert_to_UTC_asci(text);
	output << padding << "Timestamp (UTC): " << text;
}

} // namespace LumiShell

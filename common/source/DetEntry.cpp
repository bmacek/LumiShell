#include "LumiShell/common/DetEntry.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(DetEntry, CAT_NAME_LUMI_DETENTRY, CAT_TYPE_LUMI_DETENTRY);

DetEntry::DetEntry()
{
}

DetEntry::~DetEntry()
{
}

void DetEntry::cat_stream_out(std::ostream& output)
{
	output.write((char*)&_entry_id, sizeof(std::uint32_t));
	CatShell::PluginDeamon::stream_object_out(output, _period);

	std::uint32_t tmp = _events.size();
	output.write((char*)&tmp, sizeof(std::uint32_t));
	for(auto& x: _events)
		CatShell::PluginDeamon::stream_object_out(output, x);
}

void DetEntry::cat_stream_in(std::istream& input)
{
	input.read((char*)&_entry_id, sizeof(std::uint32_t));
	_period = CatShell::PluginDeamon::stream_object_in(input);

	std::uint32_t tmp;
	CatPointer<DetEvent> obj;

	_events.clear();
	input.read((char*)&tmp, sizeof(std::uint32_t));
	for(std::uint32_t i=0; i<tmp; ++i)
	{
		obj = CatShell::PluginDeamon::stream_object_in(input);
		_events.push_back(obj);
	}
}

void DetEntry::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding); output << "Lumi entry." << endl;
	output << padding << "Entry ID : " << _entry_id << endl;
	output << padding << "Events   : "  << _events.size() << endl;
	for(auto& x: _events)
	{
		x->cat_print(output, padding + " |  "); output << endl;
	}
	output << padding << "Period   :" << endl;
	_period->cat_print(output, padding + " |  ");
}

void DetEntry::cat_free() 
{
	_period.make_nullptr();
	_events.clear();
}

StreamSize DetEntry::cat_stream_get_size() const
{
	StreamSize size = sizeof(std::uint32_t);
	size += CatShell::PluginDeamon::stream_object_size(_period);
	for(auto& x: _events)
		size += CatShell::PluginDeamon::stream_object_size(x);

	return size;
}

void* DetEntry::cat_get_part(CatAddress& addr)
{
        if(addr == "id")
                return &_entry_id;
        else if(addr == "period")
                return _period->cat_get_part(++addr);
        else if(addr == "event")
        {
                if(addr.has_index())
                {
                        if(addr.get_index() >= _events.size())
                                return nullptr;
                        else
                                return _events[addr.get_index()]->cat_get_part(++addr);
                }
                else
                        return nullptr;
        }
        else
                return nullptr;
}

} // namespace LumiShell

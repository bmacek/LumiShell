#include "LumiShell/common/DetEntry.h"
#include "LumiShell/common/DetEvent.h"
#include "LumiShell/common/DetLbChange.h"
#include "LumiShell/common/defines.h"

#include "LumiShell/analysis/CorrectionMuOr.h"

#include "CatShell/core/PluginDeamon.h"

#include <cmath>

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(CorrectionMuOr, CAT_NAME_LUMI_CORRECTIONMUOR, CAT_TYPE_LUMI_CORRECTIONMUOR);


CorrectionMuOr::CorrectionMuOr()
{
}

CorrectionMuOr::~CorrectionMuOr()
{
}

CatPointer<DetEntry> CorrectionMuOr::mu_correct(CatPointer<DetEntry>& data)
{
        // mu-corrected entry
        CatPointer<DetEntry>   translated_entry = _pool_DetEntry->get_element();
        // transcribe all events
        data->loop_events([&](CatPointer<DetEvent>& e){
                translated_entry->insert_event(e);
        }); 
        // period type
        if(data->get_period()->cat_type() == CAT_TYPE_LUMI_DETLUMIMAP)
        {
                CatPointer<DetLumiMap> translated_map = _pool_DetLumiMap->get_element();
                CatPointer<DetLumiMap> original_map = data->get_period().unsafeCast<DetLumiMap>();

                // transcribe the period
                translated_map->set_start(original_map->get_start());
                translated_map->set_end(original_map->get_end());

                // map id & orbits
                translated_map->set_map_id(original_map->get_map_id());
                translated_map->set_orbits(original_map->get_orbits());

                double rate = 0;

                // loop through all the BCs and mu-correct
                for(int i=0; i<ORBIT_LENGTH; ++i)
                {

                        rate = original_map->get_value(i);
                        translated_map->get_value(i) = -log(1-rate);
                        if(rate != 0)
                                translated_map->get_error(i) = original_map->get_error(i)/(1-rate);
                        else
                        {
                                translated_map->get_error(i) = translated_map->get_value(i);
                                //WARNING(logger, "OR-based mu-correction with 0 input rate.")
                        }
                }


                translated_entry->get_period() = translated_map;
                // set id
                translated_entry->set_entry_id(data->get_entry_id());
        }
        else
        {
                // can not mu-correct -> only transcribe
                translated_entry->get_period() = data->get_period();
                // set id
                translated_entry->set_entry_id(data->get_entry_id());
        }

        return translated_entry;
}

void CorrectionMuOr::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_map && object.isSomething())
        {
                CatPointer<DetEntry>   data = object.unsafeCast<DetEntry>();

                // mu-corrected entry
                CatPointer<DetEntry>   translated_entry = mu_correct(data);

                publish_data(_out_port_map, translated_entry, logger);
        }

        Algorithm::process_data(port, object, flush, logger);
}

void CorrectionMuOr::algorithm_init()
{
        Algorithm::algorithm_init();

        // prepare the pool
        _pool_DetEntry = PluginDeamon::get_pool<DetEntry>();
        _pool_DetLumiMap = PluginDeamon::get_pool<DetLumiMap>();
        // prepare the ports
        _in_port_map = declare_input_port("in_entry", CAT_NAME_LUMI_DETENTRY);
        _out_port_map = declare_output_port("out_entry", CAT_NAME_LUMI_DETENTRY);
}

void CorrectionMuOr::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

CatPointer<CatObject> CorrectionMuOr::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        if(port == _out_port_map)
        {
                // get the source data
                CatPointer<DetEntry> data = request_data(_in_port_map, flush, logger).unsafeCast<DetEntry>();
                CatPointer<DetEntry> translated_entry(nullptr);

                if(data.isSomething())
                {
                        translated_entry = mu_correct(data);
                }

                return translated_entry;
        }

        return CatPointer<DetEntry>(nullptr);
}


} // namespace LumiShell

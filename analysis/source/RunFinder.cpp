#include "LumiShell/common/DetEntry.h"
#include "LumiShell/common/DetEvent.h"
#include "LumiShell/common/DetLbChange.h"
#include "LumiShell/common/defines.h"

#include "LumiShell/analysis/RunFinder.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(RunFinder, CAT_NAME_LUMI_RUNFINDER, CAT_TYPE_LUMI_RUNFINDER);


RunFinder::RunFinder()
{

}

RunFinder::~RunFinder()
{

}

void RunFinder::check_for_run(CatPointer<DetEntry>& data, CatShell::Logger& logger)
{
        CatPointer<DetEvent>   event;

        // check if this entry has this type of event
        data->loop_events([&](CatPointer<DetEvent>& e){
                if(e->cat_type() == CAT_TYPE_LUMI_DETLBCHANGE)
                {
                        // lb change
                        CatPointer<DetLbChange> lb = e.unsafeCast<DetLbChange>();
                        if(_current_run != lb->get_run())
                        {
                                // run changed
                                _current_run = lb->get_run();
                                // log
                                INFO(logger, "Found new run: " << _current_run);
                                // get the message
                                CatPointer<CatMessage> msg = _pool->get_element();
                                // construct text
                                char text[100]; sprintf(text, "run_%u", _current_run);
                                msg->set(CAT_MESSAGE_TYPE_UNKNOWN, text);
                                //
                                flush_data_clients(_out_port_data, logger);
                                // publish message
                                publish_data(_out_port_message, msg, logger);
                        }
                }
        }); 
}

void RunFinder::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_map && object.isSomething())
        {
                CatPointer<DetEntry>   data = object.unsafeCast<DetEntry>();

                check_for_run(data, logger);

                publish_data(_out_port_data, object, logger);
        }

        Algorithm::process_data(port, object, flush, logger);
}

void RunFinder::algorithm_init()
{
        Algorithm::algorithm_init();

        // prepare the pool
        _pool = PluginDeamon::get_pool<CatMessage>();
        // prepare the ports
        _in_port_map = declare_input_port("in_entry", CAT_NAME_LUMI_DETENTRY);
        _out_port_message = declare_output_port("out_message", CAT_NAME_MESSAGE);
        _out_port_data = declare_output_port("out_entry", CAT_NAME_LUMI_DETENTRY);

        // set initial values
        _current_run = 0;
}

void RunFinder::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

CatPointer<CatObject> RunFinder::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        if(port == _out_port_data)
        {
                // get the source data
                CatPointer<DetEntry> data = request_data(_in_port_map, flush, logger).unsafeCast<DetEntry>();

                if(data.isSomething())
                {
                        check_for_run(data, logger);
                }

                return data;
        }

        return CatPointer<DetEntry>(nullptr);
}

} // namespace LumiShell

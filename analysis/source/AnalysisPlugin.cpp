#include "CatShell/core/Plugin.h"
#include "LumiShell/analysis/RunFinder.h"
#include "LumiShell/analysis/CorrectionMuOr.h"
#include "LumiShell/analysis/MapSync.h"

using namespace std;

namespace LumiShell {

EXPORT_PLUGIN(CatShell::Plugin, LumiAnalysisPlugin)
START_EXPORT_CLASSES(LumiAnalysisPlugin)
EXPORT_CLASS(RunFinder, CAT_NAME_LUMI_RUNFINDER, LumiAnalysisPlugin)
EXPORT_CLASS(CorrectionMuOr, CAT_NAME_LUMI_CORRECTIONMUOR, LumiAnalysisPlugin)
EXPORT_CLASS(MapSync, CAT_NAME_LUMI_MAPSYNC, LumiAnalysisPlugin)
END_EXPORT_CLASSES(LumiAnalysisPlugin)

} // namespace LumiShell
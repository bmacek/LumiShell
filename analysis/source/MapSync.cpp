#include "LumiShell/analysis/MapSync.h"

#include "CatShell/core/PluginDeamon.h"

#include <cmath>

using namespace std;
using namespace CatShell;

namespace LumiShell {

CAT_OBJECT_IMPLEMENT(MapSync, CAT_NAME_LUMI_MAPSYNC, CAT_TYPE_LUMI_MAPSYNC);


MapSync::MapSync()
{
}

MapSync::~MapSync()
{
}

void MapSync::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        // nothing, since pushing the data into MapSync is not supported
}

void MapSync::algorithm_init()
{
        Process::algorithm_init();

        // read the number of sources
        _number_of_sources = algo_settings().get_setting<std::uint32_t>("sources", 0);

        char name[100];
        // open all the input ports
        for(std::uint8_t i=0; i<_number_of_sources; ++i)
        {
                sprintf(name, "in_source_%02u", (unsigned short)i); // construct the port name
                _in_port_map.push_back(declare_input_port(name, CAT_NAME_LUMI_DETENTRY));
                // reserve space in the input data
                _entries.push_back(CatPointer<DetEntry>(nullptr));
        }

        // create the output port
        _out_port = declare_output_port("sync_data", CAT_NAME_CATCOLLECTION);

        // prepare the pool
        _pool = PluginDeamon::get_pool<CatCollection>();
}

bool MapSync::fill_next(CatShell::Logger& logger)
{
        bool flush;

        // get one entry from each source
        for(std::uint8_t i=0; i<_number_of_sources; ++i)
        {
                _entries[i] = request_data(_in_port_map[i], flush, logger).unsafeCast<DetEntry>();
                if(flush)
                {
                        INFO(logger, "No elements could be extracted form source " << (int)i << "!");
                        return false;
                }
        }

        return true;
}

std::uint32_t MapSync::find_highest_id()
{
        std::uint32_t id = _entries[0]->get_entry_id();
        for(std::uint8_t i=1; i<_number_of_sources; ++i)
        {
                if(_entries[i]->get_entry_id() > id)
                        id = _entries[i]->get_entry_id();
        }        

        return id;
}

bool MapSync::advance_to_id(std::uint32_t id, CatShell::Logger& logger)
{
        bool flush;

        while(true)
        {
                // not supported
                for(std::uint8_t i=0; i<_number_of_sources; ++i)
                {
                        //std::uint32_t current_id = _entries[i]->get_entry_id();
                        // pool data until id is greater
                        while(_entries[i]->get_entry_id() < id)
                        {
                                // get next one
                                _entries[i] = request_data(_in_port_map[i], flush, logger).unsafeCast<DetEntry>();
                                if(flush)
                                {
                                        INFO(logger, "No elements could be extracted form source " << (int)i << "!");
                                        return false;
                                }
                        }

                        // check that we haven't gone too far
                        if(_entries[i]->get_entry_id() > id)
                        {
                                // entry ID is bigger ?!
                                id = _entries[i]->get_entry_id();
                                // the target ID has yust increased
                                break;
                        }

                }                        

                return true;
        }

        return false;
}

CatPointer<CatCollection> MapSync::create_collection()
{
        CatPointer<CatCollection> coll = _pool->get_element();

        for(std::uint8_t i=0; i<_number_of_sources; ++i)
                coll->push_back(_entries[i]);

        return coll;
}


void MapSync::algorithm_deinit()
{
        Process::algorithm_deinit();
}

CatPointer<CatObject> MapSync::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        bool success;
        std::uint32_t id;

        // get next round of entries
        success = fill_next(logger);
        if(!success)
        {
                flush = true;
                return CatPointer<CatObject>(nullptr);
        }

        // find the highest ID
        id = find_highest_id();

        // advance all to the same ID
        success = advance_to_id(id, logger);
        if(!success)
        {
                flush = true;
                return CatPointer<CatObject>(nullptr);
        }

        // get collection
        return create_collection();
}

void MapSync::work_main()
{
        bool success;
        std::uint32_t id;
        CatPointer<CatCollection> collection;


        if(!_number_of_sources)
        {
                INFO(logger(), "No sources registered. Exiting MapSync.");
                return;

        }

        INFO(logger(), "Starting Sync process with " << (int)_number_of_sources << " sources.");

        while(true)
        {
                if(!should_continue(false))
                        break;

                // get next round of entries
                success = fill_next(logger());
                if(!success)
                        break;

                // find the highest ID
                id = find_highest_id();

                // advance all to the same ID
                success = advance_to_id(id, logger());
                if(!success)
                        break;

                // get collection
                collection = create_collection();

                // publish the collection
                publish_data(_out_port, collection, logger());
        }
        INFO(logger(), "Sync process ended.");
}

void MapSync::work_init()
{

}

void MapSync::work_deinit()
{

}


} // namespace LumiShell

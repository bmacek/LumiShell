#ifndef INCLUDE_LUMI_MAPSYNC
#define INCLUDE_LUMI_MAPSYNC

#include "CatShell/core/Process.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatCollection.h"

#include "LumiShell/common/DetEntry.h"

#include "LumiShell/analysis/defines.h"

namespace LumiShell {

class MapSync : public CatShell::Process
{
        CAT_OBJECT_DECLARE(MapSync, CatShell::Process, CAT_NAME_LUMI_MAPSYNC, CAT_TYPE_LUMI_MAPSYNC);
public:

        MapSync();
                /// Constructor: default.

        virtual ~MapSync();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------
protected:
private:

        bool fill_next(CatShell::Logger& logger);
                /// Fills internal data with the next data.

        std::uint32_t find_highest_id();
                /// Returns highest ID in the data.

        bool advance_to_id(std::uint32_t id, CatShell::Logger& logger);
                /// Advances all entries to the desired ID.

        CatShell::CatPointer<CatShell::CatCollection> create_collection();
                /// Creates a collection form the current data.

private:

        std::vector<CatShell::CatPointer<DetEntry>>     _entries;      // current entries

        CatShell::MemoryPool<CatShell::CatCollection>*  _pool;

        std::uint8_t                                    _number_of_sources;
        std::vector<CatShell::Algorithm::DataPort>      _in_port_map;
        CatShell::Algorithm::DataPort                   _out_port;
};

inline StreamSize MapSync::cat_stream_get_size() const { return CatShell::Process::cat_stream_get_size(); }
inline void MapSync::cat_stream_out(std::ostream& output) { return CatShell::Process::cat_stream_out(output); }
inline void MapSync::cat_stream_in(std::istream& input) { return CatShell::Process::cat_stream_in(input); }
inline void MapSync::cat_print(std::ostream& output, const std::string& padding) { CatShell::Process::cat_print(output, padding); }
inline void MapSync::cat_free() { }
inline void MapSync::work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child) { }

} // namespace LumiShell

#endif


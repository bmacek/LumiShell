#ifndef INCLUDE_LUMIANALYSIS_DEFINES
#define INCLUDE_LUMIANALYSIS_DEFINES

#include "LumiShell/common/defines.h"

//
// constants
//

//
// types
//

#define CAT_TYPE_LUMI_RUNFINDER			(0x00001023)
#define CAT_NAME_LUMI_RUNFINDER			"RunFinder"
#define CAT_TYPE_LUMI_CORRECTIONMUOR	(0x00001027)
#define CAT_NAME_LUMI_CORRECTIONMUOR	"CorrectionMuOr"
#define CAT_TYPE_LUMI_MAPSYNC			(0x00001028)
#define CAT_NAME_LUMI_MAPSYNC			"MapSync"


#endif

#ifndef INCLUDE_LUMI_CORRECTIONMUOR
#define INCLUDE_LUMI_CORRECTIONMUOR

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "LumiShell/common/DetEntry.h"
#include "LumiShell/common/DetLumiMap.h"

#include "LumiShell/analysis/defines.h"

namespace LumiShell {

class CorrectionMuOr : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(CorrectionMuOr, CatShell::Algorithm, CAT_NAME_LUMI_CORRECTIONMUOR, CAT_TYPE_LUMI_CORRECTIONMUOR);
public:

        CorrectionMuOr();
                /// Constructor: default.

        virtual ~CorrectionMuOr();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------

protected:
private:

        CatShell::CatPointer<DetEntry> mu_correct(CatShell::CatPointer<DetEntry>& data);
                /// Corrects for mu-nonlinearities.

private:

        CatShell::MemoryPool<LumiShell::DetEntry>*      _pool_DetEntry;
        CatShell::MemoryPool<LumiShell::DetLumiMap>*    _pool_DetLumiMap;

        CatShell::Algorithm::DataPort   _in_port_map;
        CatShell::Algorithm::DataPort   _out_port_map;
};

inline StreamSize CorrectionMuOr::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void CorrectionMuOr::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void CorrectionMuOr::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void CorrectionMuOr::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void CorrectionMuOr::cat_free() { }

} // namespace LumiShell

#endif


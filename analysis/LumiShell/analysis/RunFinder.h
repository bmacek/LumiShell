#ifndef INCLUDE_BCM_LUMIRUNFINDER
#define INCLUDE_BCM_LUMIRUNFINDER

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "CatShell/basics/CatMessage.h"

#include "LumiShell/common/DetEntry.h"

#include "LumiShell/analysis/defines.h"

namespace LumiShell {

class RunFinder : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(RunFinder, CatShell::Algorithm, CAT_NAME_LUMI_RUNFINDER, CAT_TYPE_LUMI_RUNFINDER);
public:

        RunFinder();
                /// Constructor: default.

        virtual ~RunFinder();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------

protected:
private:
        void check_for_run(CatShell::CatPointer<DetEntry>& data, CatShell::Logger& logger);
                /// Checks if new run occured and publishes message if needed.


private:
        CatShell::MemoryPool<CatShell::CatMessage>* _pool;

        CatShell::Algorithm::DataPort   _in_port_map;
        CatShell::Algorithm::DataPort   _out_port_message;
        CatShell::Algorithm::DataPort   _out_port_data;

        std::uint32_t   _current_run;
};

inline StreamSize RunFinder::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void RunFinder::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void RunFinder::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void RunFinder::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void RunFinder::cat_free() { }

} // namespace LumiShell

#endif

